<?php
define("NO_KEEP_STATISTIC", true);
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");


use Bitrix\Main\Loader;
use Bitrix\Highloadblock as HL;

Loader::includeModule("highloadblock"); 


$arLangs = Array(
    'ru' => 'Посты',
    'en' => 'Publications'
);

$result = HL\HighloadBlockTable::add(array(
    'NAME' => 'Publications',
    'TABLE_NAME' => 'test_publications',
));

if ($result->isSuccess()) {
    $id = $result->getId();
    foreach($arLangs as $lang_key => $lang_val){
        HL\HighloadBlockLangTable::add(array(
            'ID' => $id,
            'LID' => $lang_key,
            'NAME' => $lang_val
        ));
    }
} else {
    $errors = $result->getErrorMessages();
    var_dump($errors);  
}


$UFObject = 'HLBLOCK_'.$id;

$arPublicationFields = Array(
    'UF_NAME'=>Array(
        'ENTITY_ID' => $UFObject,
        'FIELD_NAME' => 'UF_NAME',
        'USER_TYPE_ID' => 'string',
        'MANDATORY' => 'Y',
        "EDIT_FORM_LABEL" => Array('ru'=>'Название', 'en'=>'Name'), 
        "LIST_COLUMN_LABEL" => Array('ru'=>'Название', 'en'=>'Name'),
        "LIST_FILTER_LABEL" => Array('ru'=>'Название', 'en'=>'Name'), 
        "ERROR_MESSAGE" => Array('ru'=>'', 'en'=>''), 
        "HELP_MESSAGE" => Array('ru'=>'', 'en'=>''),
    ),
);


$arSavedFieldsRes = Array();
foreach($arPublicationFields as $arPublicationField){
    $obUserField  = new CUserTypeEntity;
    $ID = $obUserField->Add($arPublicationField);
    $arSavedFieldsRes[] = $ID;
}

if($arSavedFieldsRes)
{
    $filter = ['=ID' => $id];
    $hlblock = HL\HighloadBlockTable::getList(['filter' => $filter])->fetch();
    $entity = HL\HighloadBlockTable::compileEntity($hlblock);
    $entity_data_class = $entity->getDataClass();

    for($i=1;$i<10;$i++){
        $data = array(
            "UF_NAME"=>"Пост №".$i,
        );
        
        $r = $entity_data_class::add($data);
    }
}