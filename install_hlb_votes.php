<?php
define("NO_KEEP_STATISTIC", true);
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");


use Bitrix\Main\Loader;
use Bitrix\Highloadblock;

Loader::includeModule("highloadblock"); 


$arLangs = Array(
    'ru' => 'Голоса',
    'en' => 'Votes'
);

$result = Highloadblock\HighloadBlockTable::add(array(
    'NAME' => 'Votes',
    'TABLE_NAME' => 'test_votes',
));

if ($result->isSuccess()) {
    $id = $result->getId();
    foreach($arLangs as $lang_key => $lang_val){
        Highloadblock\HighloadBlockLangTable::add(array(
            'ID' => $id,
            'LID' => $lang_key,
            'NAME' => $lang_val
        ));
    }
} else {
    $errors = $result->getErrorMessages();
    var_dump($errors);  
}


$UFObject = 'HLBLOCK_'.$id;

$arVoteFields = Array(
    'UF_ID_PUBLICATION'=>Array(
        'ENTITY_ID' => $UFObject,
        'FIELD_NAME' => 'UF_ID_PUBLICATION',
        'USER_TYPE_ID' => 'integer',
        'MANDATORY' => 'Y',
        "EDIT_FORM_LABEL" => Array('ru'=>'ID поста', 'en'=>'ID publication'), 
        "LIST_COLUMN_LABEL" => Array('ru'=>'ID поста', 'en'=>'ID publication'),
        "LIST_FILTER_LABEL" => Array('ru'=>'ID поста', 'en'=>'ID publication'), 
        "ERROR_MESSAGE" => Array('ru'=>'', 'en'=>''), 
        "HELP_MESSAGE" => Array('ru'=>'', 'en'=>''),
    ),
    'UF_IP_ADRESS'=>Array(
        'ENTITY_ID' => $UFObject,
        'FIELD_NAME' => 'UF_IP_ADRESS',
        'USER_TYPE_ID' => 'string',
        'MANDATORY' => 'Y',
        "EDIT_FORM_LABEL" => Array('ru'=>'IP адрес', 'en'=>'IP adress'), 
        "LIST_COLUMN_LABEL" => Array('ru'=>'IP адрес', 'en'=>'IP adress'),
        "LIST_FILTER_LABEL" => Array('ru'=>'IP адрес', 'en'=>'IP adress'), 
        "ERROR_MESSAGE" => Array('ru'=>'', 'en'=>''), 
        "HELP_MESSAGE" => Array('ru'=>'', 'en'=>''),
    ),
    'UF_DATE'=>Array(
        'ENTITY_ID' => $UFObject,
        'FIELD_NAME' => 'UF_DATE',
        'USER_TYPE_ID' => 'datetime',
        'MANDATORY' => '',
        "EDIT_FORM_LABEL" => Array('ru'=>'Дата и время', 'en'=>'datetime'), 
        "LIST_COLUMN_LABEL" => Array('ru'=>'Дата и время', 'en'=>'datetime'),
        "LIST_FILTER_LABEL" => Array('ru'=>'Дата и время', 'en'=>'datetime'), 
        "ERROR_MESSAGE" => Array('ru'=>'', 'en'=>''), 
        "HELP_MESSAGE" => Array('ru'=>'', 'en'=>''),
    ),
);


$arSavedFieldsRes = Array();
foreach($arVoteFields as $arVoteField){
    $obUserField  = new CUserTypeEntity;
    $ID = $obUserField->Add($arVoteField);
    $arSavedFieldsRes[] = $ID;
}

var_dump($arSavedFieldsRes);