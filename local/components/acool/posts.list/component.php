<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true)die();

use Bitrix\Main\Loader,
	Bitrix\Highloadblock as HL,
	Bitrix\Main\Entity;

Loader::includeModule("highloadblock");

\CBitrixComponent::includeComponentClass('acool:posts.vote');

$voteClass = new PostVoteComponent();

// hlblock
$hlblock_id = $arParams['BLOCK_ID'];
$hlblock_name = $arParams['BLOCK_NAME'];

if ($hlblock_id) {
	$filter = ['=ID' => $hlblock_id];
}
elseif ($hlblock_name) {
	$filter = ['=NAME' => $hlblock_name];
}
else{
	ShowError(GetMessage('HLBLOCK_LIST_NO_ID'));
	return 0;
}

$hlblock = HL\HighloadBlockTable::getList(['filter' => $filter])->fetch();
//$hlblock = HL\HighloadBlockTable::getById($hlblock_id)->fetch();
if(!$hlblock)
{
	ShowError(GetMessage('HLBLOCK_LIST_404'));
	return 0;
}


$entity = HL\HighloadBlockTable::compileEntity($hlblock);

$entity_data_class = $entity->getDataClass();

// sort
$sortId = 'ID';
$sortType = 'DESC';
if (isset($arParams['SORT_FIELD']) && isset($fields[$arParams['SORT_FIELD']]))
{
	$sortId = $arParams['SORT_FIELD'];
}
elseif (isset($_GET['sort_id']) && isset($fields[$_GET['sort_id']]))
{
	$sortId = $_GET['sort_id'];
}

if (isset($arParams['SORT_ORDER']) && in_array($arParams['SORT_ORDER'], array('ASC', 'DESC'), true))
{
	$sortType = $arParams['SORT_ORDER'];
}

if (isset($_GET['sort_type']) && in_array($_GET['sort_type'], array('ASC', 'DESC'), true))
{
	$sortType = $_GET['sort_type'];
}

// arResult
$data = $entity_data_class::getList(array(
	"select" => array("*"),
	"order" => array($sortId => $sortType),
	"cache" => ['ttl' => 3600],
));

while($arData = $data->Fetch()){	
	$arResult["ELEMENTS"][] = array(
		"ID" => $arData["ID"],
		"NAME" => $arData["UF_NAME"],
		"VOTE_COUNT" => $voteClass->getCountAction($arData["ID"])
	);
}


$this->IncludeComponentTemplate();