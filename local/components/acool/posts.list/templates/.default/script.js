const items = document.querySelectorAll(".post");

for (let i = 0; i < items.length; i++) {
    items[i].addEventListener('click', function () {
        var id = items[i].dataset.id;
        var count_block = items[i].querySelector(".count");
        
        BX.ajax.runComponentAction('acool:posts.vote', 'add', {
            mode: 'class',
            data: {
                id: id
            }	
        }).then(function (response) {
            BX.ajax.runComponentAction('acool:posts.vote', 'getCount', {
                mode: 'class',
                data: {
                    id: id
                }	
            }).then(function (response) {
                count_block.innerText = response.data;
            }, function (response) {
                console.log(response);           				
            });
        }, function (response) {
            console.log(response);           				
        });
    });   
}