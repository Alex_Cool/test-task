<?

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if (!empty($arResult['ERROR']))
{
	echo $arResult['ERROR'];
	return false;
}
?>

<ul>
	<?foreach($arResult['ELEMENTS'] as $elem): ?>
		<li data-id="<?=$elem["ID"]?>" class="post"><?=$elem["NAME"]?> (<span class="count"><?=$elem["VOTE_COUNT"]?></span>)</li>
	<?endforeach;?>
</ul>