<?php

use Bitrix\Main\Result;
use Bitrix\Main\Error;
use Bitrix\Main\ErrorCollection;
use Bitrix\Main\Type\DateTime;
use Bitrix\Main\Loader,
	Bitrix\Highloadblock as HL,
	Bitrix\Main\Entity;
use \Bitrix\Main\Service\GeoIp;  

class PostVoteComponent extends \CBitrixComponent implements \Bitrix\Main\Engine\Contract\Controllerable, \Bitrix\Main\Errorable
{
    private $hlbName = "Votes";

    private $entity;

    protected $errorCollection;

    protected $userIp;

    public function __construct(CBitrixComponent $component = null)
    {
        parent::__construct($component);

        $this->checkModules();

        $this->entity = $this->getEntityDataClass();

        $this->userIp = $this->getIp();
    }

    private function checkModules() : void
    {
        if (!Loader::includeModule('highloadblock')) {
            throw new \Exception('Не загружен модуль highloadblock');
        }
    }

    protected function getEntityDataClass()
    {
        if ($this->hlbName) {
            $filter = ['=NAME' => $this->hlbName];
        }
        else{
            throw new \Exception('Не корректно указан highloadblock');
        }

        $hlblock = HL\HighloadBlockTable::getList(['filter' => $filter])->fetch();
       
        if(!$hlblock)
        {
            throw new \Exception('Указанный highloadblock не найден');
        }

        $entity = HL\HighloadBlockTable::compileEntity($hlblock);

        return $entity->getDataClass(); 

    }

    public function configureActions()
	{
		return [];
	}

    public function onPrepareComponentParams($arParams)
	{
		$this->errorCollection = new ErrorCollection();
	}

    public function getCountAction(int $id)
    {
        $result = [];

        $data = $this->entity::getList(array(
            "select" => array("*"),
            "filter" => array("UF_ID_PUBLICATION"=>$id)
        ));
         
        while($arData = $data->Fetch()){
            $result[] = $arData;
        }

        return count($result);
    }

    public function addAction(int $id)
    {
        $result = new Result();

        if(!$this->existIp($id)){
            $data = array(
                "UF_ID_PUBLICATION"=>$id,
                "UF_IP_ADRESS"=>$this->userIp,
                "UF_DATE"=>new DateTime()
            );
            
            $r = $this->entity::add($data);
            
            if (!$r->isSuccess()) 
            {
                $result->addErrors($r->getErrors());
            }
        }

        return $result;
    }

    public function getIp()
    {
        return GeoIp\Manager::getRealIp();
    }

    public function existIp($id)
    {
        $data = $this->entity::getList(array(
            "select" => array("*"),
            "filter" => array("UF_ID_PUBLICATION"=>$id,"UF_IP_ADRESS"=>$this->userIp)
        ));
         
        if($arData = $data->Fetch()){
            return true;
        }

        return false;
    }

    /**
	 * Getting array of errors.
	 * @return Error[]
	 */
	public function getErrors()
	{
		return $this->errorCollection->toArray();
	}
	/**
	 * Getting once error with the necessary code.
	 * @param string $code Code of error.
	 * @return Error
	 */
	public function getErrorByCode($code)
	{
		return $this->errorCollection->getErrorByCode($code);
	}

    public function executeComponent()
    {
        if($this->startResultCache())  
        {
            $this->arResult["Y"] = $this->sqr($this->arParams["X"]);
            $this->includeComponentTemplate();
        }
        return $this->arResult["Y"];
    }
}